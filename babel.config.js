module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          '@api': './src/api',
          '@assets': './src/assets',
          '@ui': './src/ui',
          '@constants': './src/constants',
          '@components': './src/components',
          '@layouts': './src/layouts',
          '@hooks': './src/hooks',
          '@navigation': './src/navigation',
          '@routes': './src/navigation/routes',
          '@screens': './src/screens',
          '@libs': './src/libs',
          '@services': './src/services',
          '@types': './src/types',
        },
      },
    ],
  ],
};
