import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { HistoryItemScreen, HistoryScreen, HomeScreen, ScanScreen, SignInScreen } from 'screens';
import { AppRoutes } from './AppRoutes';
import { DEFAULT_SCREEN_OPTIONS } from './Navigator.options';
import { ScreenBaseProps } from 'types';
import { useInitApp } from 'hooks/useInitApp';
import { FullScreenLoading } from 'ui/FullScreenLoading/FullScreenLoading';

const MainStack = createNativeStackNavigator<NavigatorParams>();

export const MainStackNavigator = () => {
  const { initRoute, isInitSuccess } = useInitApp();
  return isInitSuccess ? (
    <NavigationContainer>
      <MainStack.Navigator initialRouteName={initRoute}>
        <MainStack.Screen
          name={AppRoutes.SignInScreen}
          component={SignInScreen}
          options={DEFAULT_SCREEN_OPTIONS}
        />
        <MainStack.Screen
          name={AppRoutes.HomeScreen}
          component={HomeScreen}
          options={DEFAULT_SCREEN_OPTIONS}
        />
        <MainStack.Screen
          name={AppRoutes.ScanScreen}
          component={ScanScreen}
          options={DEFAULT_SCREEN_OPTIONS}
        />
        <MainStack.Screen
          name={AppRoutes.HistoryScreen}
          component={HistoryScreen}
          options={DEFAULT_SCREEN_OPTIONS}
        />
        <MainStack.Screen
          name={AppRoutes.HistoryItemScreen}
          component={HistoryItemScreen}
          options={DEFAULT_SCREEN_OPTIONS}
        />
      </MainStack.Navigator>
    </NavigationContainer>
  ) : (
    <FullScreenLoading />
  );
};

export type NavigatorParams = {
  [AppRoutes.SignInScreen]: undefined;
  [AppRoutes.HomeScreen]: undefined;
  [AppRoutes.ScanScreen]: undefined;
  [AppRoutes.HistoryScreen]: undefined;
  [AppRoutes.HistoryItemScreen]: { itemId: number };
};

export type SignInScreenProps = ScreenBaseProps<AppRoutes.SignInScreen, NavigatorParams>;
export type HomeScreenProps = ScreenBaseProps<AppRoutes.HomeScreen, NavigatorParams>;
export type ScanScreenProps = ScreenBaseProps<AppRoutes.ScanScreen, NavigatorParams>;
export type HistoryScreenProps = ScreenBaseProps<AppRoutes.HistoryScreen, NavigatorParams>;
export type HistoryItemScreenProps = ScreenBaseProps<AppRoutes.HistoryItemScreen, NavigatorParams>;
