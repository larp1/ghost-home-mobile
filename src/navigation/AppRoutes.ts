export enum AppRoutes {
  SignInScreen = 'SignInScreen',
  HomeScreen = 'HomeScreen',
  ScanScreen = 'ScanScreen',
  HistoryScreen = 'HistoryScreen',
  HistoryItemScreen = 'HistoryItemScreen',
}
