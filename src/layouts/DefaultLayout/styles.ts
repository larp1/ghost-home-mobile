import { StyleSheet } from 'react-native';
import { colors } from 'styles';

export default StyleSheet.create({
  root: {
    backgroundColor: colors.background,
    flex: 1,
  },
  withHorizontalPadding: {
    paddingHorizontal: 16,
  },
});
