import { StyleSheet } from 'react-native';
import { colors } from 'styles';

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: 40,
    marginTop: 0,
  },
  leftButton: {
    position: 'absolute',
    left: 0,
    width: 40,
    height: 40,
  },
  rightButton: {
    position: 'absolute',
    right: 0,
  },
  title: {
    paddingHorizontal: 45,
    textAlign: 'center',
    color: colors.text,
    fontSize: 30,
    fontWeight: '700',
  },
});

export default styles;
