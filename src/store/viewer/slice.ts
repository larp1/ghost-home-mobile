import { createSlice } from '@reduxjs/toolkit';
import { RootState } from 'store';
import { Viewer } from 'types';
import { getMe, login } from './asyncActions';

type ViewerState = {
  token: string | null;
  user: Viewer | null;
};

const initialState: ViewerState = {
  token: null,
  user: null,
};

const viewerSlice = createSlice({
  name: 'viewer',
  initialState,
  reducers: {
    logOut(state) {
      state.token = null;
      state.user = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(login.fulfilled, (state, { payload }) => {
      state.token = payload;
    });
    builder.addCase(getMe.fulfilled, (state, { payload }) => {
      state.user = payload;
    });
  },
});

export const selectors = {
  selectToken: (state: RootState) => state.viewer.token,
  selectViewer: (state: RootState) => state.viewer.user,
};

export const { reducer } = viewerSlice;
export const actions = { ...viewerSlice.actions, login, getMe };
