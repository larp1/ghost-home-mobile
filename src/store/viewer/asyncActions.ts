import { createAsyncThunk } from '@reduxjs/toolkit';
import { api } from 'api';

export const login = createAsyncThunk('viewer/login', async (userId: number) => {
  try {
    const { data } = await api.login(userId);
    return data;
  } catch (err) {
    throw err;
  }
});

export const getMe = createAsyncThunk('viewer/get-me', async () => {
  try {
    const { data } = await api.getMe();
    return data;
  } catch (err) {
    throw err;
  }
});
