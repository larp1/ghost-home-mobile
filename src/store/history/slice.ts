import { createSlice } from '@reduxjs/toolkit';
import { RootState } from 'store';
import { HistoryItemInStore } from 'types';
import { getHistoryItem } from './asyncActions';

type HistoryState = {
  items: HistoryItemInStore[];
  scannedItems: number[];
};

const initialState: HistoryState = {
  items: [],
  scannedItems: [],
};

const historySlice = createSlice({
  name: 'history',
  initialState,
  reducers: {
    logOut(state) {
      state.items = [];
      state.scannedItems = [];
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getHistoryItem.fulfilled, (state, { payload }) => {
      console.log('payload', payload);
      state.items.push(payload);
      state.scannedItems.push(payload.id);
    });
  },
});

export const selectors = {
  selectAllItems: (state: RootState) => state.history.items,
  selectItemById: (id: number) => (state: RootState) => state.history.items.find((item) => item.id === id),
  selectScannedIds: (state: RootState) => state.history.scannedItems,
};

export const { reducer } = historySlice;
export const actions = { ...historySlice.actions, getHistoryItem };
