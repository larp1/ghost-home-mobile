import { createAsyncThunk } from '@reduxjs/toolkit';
import { api } from 'api';

export const getHistoryItem = createAsyncThunk(
  'history/get-history-item',
  async ({ path, itemId }: { path: string; itemId: number }) => {
    try {
      const { data } = await api.getHistoryItem(path);
      return { id: itemId, ...data };
    } catch (err) {
      throw err;
    }
  },
);
