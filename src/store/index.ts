import AsyncStorage from '@react-native-async-storage/async-storage';
import { AnyAction, configureStore } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';
import { persistReducer, persistStore } from 'redux-persist';

import { actions, reducer, selectors } from './ducks';

export { actions, selectors };

export type State = ReturnType<typeof reducer>;

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

export const rootReducer = (state: State, action: AnyAction) => {
  return reducer(state, action);
};

const persistedReducer = persistReducer(persistConfig, rootReducer as typeof reducer);

export const store = configureStore({
  reducer: persistedReducer,
  devTools: true,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export const persistor = persistStore(store);

export type RootState = ReturnType<typeof reducer>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
