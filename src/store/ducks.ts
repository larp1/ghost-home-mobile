import { combineReducers } from 'redux';

import * as viewer from './viewer';
import * as history from './history';

export const reducer = combineReducers({
  viewer: viewer.reducer,
  history: history.reducer,
});

export const actions = {
  viewer: viewer.actions,
  history: history.actions,
};

export const selectors = {
  viewer: viewer.selectors,
  history: history.selectors,
};
