import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { RootState } from 'store';

type ExampleState = {
  exampleState: string | null;
};

const initialState: ExampleState = {
  exampleState: null,
};

const exampleSlice = createSlice({
  name: 'example',
  initialState,
  reducers: {
    setExampleState(state, { payload }: PayloadAction<string>) {
      state.exampleState = payload;
    },
  },
});

export const selectors = {
  selectExampleState: (state: RootState) => state,
};

export const { reducer } = exampleSlice;
export const actions = { ...exampleSlice.actions };
