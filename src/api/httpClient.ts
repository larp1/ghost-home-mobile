import axios from 'axios';
import { API_BASE_URL } from 'constants';
import { selectors, store as Store } from 'store';

export const httpClient = axios.create({
  baseURL: API_BASE_URL,
});

export const AxiosInterceptors = {
  setup: (store: typeof Store) => {
    httpClient.interceptors.request.use(
      async (config) => {
        const state = store?.getState();
        const accessToken = state ? selectors.viewer.selectToken(state) : null;

        if (!accessToken) {
          return config;
        }

        const headers = {
          ...config.headers,
          token: `${accessToken}`,
        };

        return { ...config, headers };
      },
      (error) => {
        return Promise.reject(error);
      },
    );

    httpClient.interceptors.response.use(
      (response) => {
        return response;
      },
      (error) => {
        if (error.response) {
          throw error.response.data;
        }

        if (error.data) {
          throw error.data;
        }

        throw error;
      },
    );
  },
};
