import { API_BASE_URL } from 'constants';
import { HistoryItem, Viewer } from 'types';

import { httpClient } from './httpClient';

const login = (userId: number) => {
  return httpClient.get<string>(`${API_BASE_URL}/login?user_id=${userId}`);
};

const getMe = () => {
  return httpClient.get<Viewer>(`${API_BASE_URL}/me`);
};

const getHistoryItem = (itemPath: string) => {
  return httpClient.get<HistoryItem>(`${API_BASE_URL}${itemPath}`);
};

export const api = {
  login,
  getMe,
  getHistoryItem,
};
