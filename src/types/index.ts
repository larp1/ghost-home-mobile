import { ParamListBase, RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

export type ScreenBaseProps<PageName extends string, NavigatorParams extends ParamListBase> = {
  navigation: NativeStackNavigationProp<NavigatorParams, PageName>;
  route: RouteProp<NavigatorParams, PageName>;
};

export type Viewer = {
  name: string;
  description: string;
  roles: { name: string }[];
};

export type HistoryItem = {
  items: {
    id: number;
    role: number;
    text: string;
    title: string;
  }[];
  links: {
    id: number;
    linked_id: number;
    owner_id: number;
    title: string;
  }[];
};

export type HistoryItemInStore = HistoryItem & {
  id: number;
};
