import { AxiosInterceptors } from 'api';
import { MainStackNavigator } from 'navigation';
import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { store } from './store';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import { colors } from 'styles';

AxiosInterceptors.setup(store);

const App = () => {
  useEffect(() => {
    changeNavigationBarColor(colors.background, false, false);
  }, []);

  return (
    <Provider store={store}>
      <MainStackNavigator />
    </Provider>
  );
};

export default App;
