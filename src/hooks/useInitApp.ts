import { AppRoutes } from 'navigation';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { selectors } from 'store';

export const useInitApp = () => {
  const token = useSelector(selectors.viewer.selectToken);
  const [isInitSuccess, setIsInitSuccess] = useState(false);

  useEffect(() => {
    setTimeout(() => setIsInitSuccess(true), 100);
  }, []);

  return {
    initRoute: token ? AppRoutes.HomeScreen : AppRoutes.SignInScreen,
    isInitSuccess,
  };
};
