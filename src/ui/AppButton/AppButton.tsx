import React, { FC } from 'react';
import { StyleSheet, TouchableOpacity, TouchableOpacityProps } from 'react-native';
import { colors } from 'styles';
import { AppText } from 'ui/AppText';

type AppButtonProps = TouchableOpacityProps & {
  text: string;
};

export const AppButton: FC<AppButtonProps> = ({ text, ...props }) => {
  return (
    <TouchableOpacity {...props} style={[styles.button, props.style]}>
      <AppText style={styles.text}>{text}</AppText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    borderRadius: 8,
    padding: 8,
    backgroundColor: colors.red,
    marginTop: 10,
  },
  text: {
    fontSize: 20,
  },
});
