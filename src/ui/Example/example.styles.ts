import { StyleSheet } from 'react-native';
import { sizes, colors } from 'styles';

const styles = StyleSheet.create({
  root: {
    width: '100%',
    height: sizes.screen.height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  shadow: {
    width: 200,
    height: 200,
    backgroundColor: '#fff',
  },
  typography: {
    marginBottom: 20,
  },
  link: {
    color: colors.text,
  },
});

export default styles;
