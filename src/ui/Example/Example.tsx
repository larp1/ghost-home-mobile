// import { useNavigation } from '@react-navigation/native';
import React, { FC, ReactNode, useMemo } from 'react';
import { Button, Text, View } from 'react-native';
import { AppRoutes } from 'navigation';

import styles from './example.styles';
import { useNavigation } from '@react-navigation/native';

type ExampleProps = {
  text: string;
  children?: ReactNode;
  links?: {
    text: string;
    link: AppRoutes;
  }[];
};

export const Example: FC<ExampleProps> = ({ text, links, children }) => {
  const navigation = useNavigation<any>();

  const renderLinks = useMemo(() => {
    return links?.map((item, index) => (
      <Button key={index} onPress={() => navigation.navigate(item.link)} title={item.text} />
    ));
  }, [links, navigation]);

  return (
    <View style={styles.root}>
      <Text style={styles.typography}>{text}</Text>
      {children}
      {renderLinks}
    </View>
  );
};
