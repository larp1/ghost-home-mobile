import React, { FC } from 'react';
import { StyleSheet, Text, TextProps } from 'react-native';
import { colors } from 'styles';

export const AppText: FC<TextProps> = ({ children, ...props }) => {
  return (
    <Text allowFontScaling={false} {...props} style={[styles.text, props.style]}>
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {
    color: colors.text,
  },
});
