import { DefaultLayout } from 'layouts';
import { AppRoutes } from 'navigation';
import { HomeScreenProps } from 'navigation/MainStackNavigator';
import React, { FC, useEffect } from 'react';
import { Alert, ScrollView, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { actions, selectors, useAppDispatch } from 'store';
import { colors } from 'styles';
import { AppButton, AppText } from 'ui';
import { LogoutIcon } from 'ui/icons';

export const HomeScreen: FC<HomeScreenProps> = ({ navigation }) => {
  const dispatch = useAppDispatch();
  const viewer = useSelector(selectors.viewer.selectViewer);

  useEffect(() => {
    dispatch(actions.viewer.getMe());
  }, [dispatch]);

  const logout = () => {
    Alert.alert('Точно выйти из профиля?', '', [
      {
        text: 'Отмена',
        style: 'cancel',
      },
      {
        text: 'Да',
        onPress: () => {
          dispatch(actions.viewer.logOut());
          dispatch(actions.history.logOut());
          navigation.reset({ routes: [{ name: AppRoutes.SignInScreen }] });
        },
      },
    ]);
  };

  return (
    <DefaultLayout>
      <DefaultLayout.Header
        title={viewer?.name}
        onRightButtonPress={logout}
        customRight={<LogoutIcon fill={colors.red} />}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <AppText style={styles.description}>{viewer?.description}</AppText>
      </ScrollView>
      <AppButton text="ДНЕВНИК" onPress={() => navigation.navigate(AppRoutes.HistoryScreen)} />
      <AppButton
        style={styles.scan}
        text="СКАНИРОВАТЬ"
        onPress={() => navigation.navigate(AppRoutes.ScanScreen)}
      />
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  description: {
    lineHeight: 25,
    fontSize: 18,
  },
  scan: {
    backgroundColor: colors.yellow,
  },
});
