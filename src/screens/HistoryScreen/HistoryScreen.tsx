import { DefaultLayout } from 'layouts';
import { AppRoutes } from 'navigation';
import { HistoryScreenProps } from 'navigation/MainStackNavigator';
import React, { FC, useCallback } from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { selectors } from 'store';
import { colors } from 'styles';
import { HistoryItemInStore } from 'types';
import { AppButton, AppText } from 'ui';

const getKey = (item: HistoryItemInStore) => item.id.toString();

export const HistoryScreen: FC<HistoryScreenProps> = ({ navigation }) => {
  const history = useSelector(selectors.history.selectAllItems);

  const renderItem = useCallback(
    ({ item }: { item: HistoryItemInStore }) => {
      const title = item.items.find((_item) => item.id === _item.id)?.title;
      return (
        <TouchableOpacity
          key={item.id}
          style={styles.item}
          onPress={() => navigation.navigate(AppRoutes.HistoryItemScreen, { itemId: item.id })}>
          <AppText style={styles.itemText}>{title}</AppText>
        </TouchableOpacity>
      );
    },
    [navigation],
  );

  return (
    <DefaultLayout>
      <DefaultLayout.Header title="ДНЕВНИК" defaultBack />
      <FlatList
        contentContainerStyle={styles.list}
        data={history}
        renderItem={renderItem}
        keyExtractor={getKey}
      />
      <AppButton
        style={styles.scan}
        text="СКАНИРОВАТЬ"
        onPress={() => navigation.navigate(AppRoutes.ScanScreen)}
      />
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  list: {
    paddingTop: 20,
  },
  item: {
    alignItems: 'center',
    padding: 10,
    marginBottom: 20,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: colors.text,
  },
  itemText: {
    fontSize: 20,
  },
  scan: {
    backgroundColor: colors.yellow,
  },
});
