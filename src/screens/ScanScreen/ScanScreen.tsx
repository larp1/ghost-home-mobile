import { DefaultLayout } from 'layouts';
import { AppRoutes } from 'navigation';
import { ScanScreenProps } from 'navigation/MainStackNavigator';
import React, { FC, useEffect, useState } from 'react';
import { Alert, StyleSheet } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { useSelector } from 'react-redux';
import { actions, selectors, useAppDispatch } from 'store';
import { colors } from 'styles';
import { AppButton } from 'ui';
import { RNCamera } from 'react-native-camera';

export const ScanScreen: FC<ScanScreenProps> = ({ navigation }) => {
  const dispatch = useAppDispatch();
  const scannedItems = useSelector(selectors.history.selectScannedIds);
  const [isShowScanner, setIsShowScanner] = useState(true);
  const [isTorchOn, setIsTorchOn] = useState(false);

  const handleFlashlight = async () => {
    setIsTorchOn(!isTorchOn);
  };

  const onScan = async (data: string) => {
    try {
      if (!data.includes('/item/')) {
        Alert.alert('Этот QR-код не сканируется!');
        return;
      }
      const num = Number(data.split('/item/')[1]);
      if (!scannedItems.includes(num)) {
        await dispatch(actions.history.getHistoryItem({ path: data, itemId: num })).unwrap();
      }
      navigation.navigate(AppRoutes.HistoryItemScreen, { itemId: num });
    } catch (err) {
      Alert.alert('Что-то пошло не так', 'Попробуй еще раз');
    }
  };

  useEffect(() => {
    if (!isShowScanner) {
      setTimeout(() => {
        setIsShowScanner(true);
      }, 0);
    }
  }, [isShowScanner]);

  return (
    <DefaultLayout withHorizontalPadding={false}>
      <DefaultLayout.Header defaultBack />
      {isShowScanner && (
        <QRCodeScanner
          onRead={(e) => onScan(e.data)}
          flashMode={isTorchOn ? RNCamera.Constants.FlashMode.torch : RNCamera.Constants.FlashMode.off}
        />
      )}
      <AppButton style={styles.light} text="СВЕТ" onPress={handleFlashlight} />
      <AppButton text="ПЕРЕЗАГРУЗИТЬ КАМЕРУ" onPress={() => setIsShowScanner(false)} />
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  light: {
    backgroundColor: colors.yellow,
  },
});
