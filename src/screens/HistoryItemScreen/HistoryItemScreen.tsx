import { DefaultLayout } from 'layouts';
import { AppRoutes } from 'navigation';
import { HistoryItemScreenProps } from 'navigation/MainStackNavigator';
import React, { FC } from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { selectors } from 'store';
import { colors } from 'styles';
import { AppButton, AppText } from 'ui';

export const HistoryItemScreen: FC<HistoryItemScreenProps> = ({ route, navigation }) => {
  const historyItem = useSelector(selectors.history.selectItemById(route.params.itemId));

  const title = historyItem?.items.find((item) => item.id === route.params.itemId)?.title;

  return (
    <DefaultLayout>
      <DefaultLayout.Header defaultBack />
      <ScrollView showsVerticalScrollIndicator={false}>
        <AppText style={styles.title}>{title}</AppText>
        {historyItem?.items.map((item) => (
          <React.Fragment key={item.id}>
            <AppText style={styles.text}>{item.text}</AppText>
            <AppText>---------- ---------- ----------</AppText>
          </React.Fragment>
        ))}
        {historyItem?.links.map((link) => (
          <React.Fragment key={link.id}>
            <AppText style={styles.text}>{link.title}</AppText>
            <AppText>---------- ---------- ----------</AppText>
          </React.Fragment>
        ))}
      </ScrollView>
      <AppButton
        style={styles.scan}
        text="СКАНИРОВАТЬ"
        onPress={() => navigation.navigate(AppRoutes.ScanScreen)}
      />
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 24,
    textAlign: 'center',
    marginBottom: 10,
  },
  text: {
    lineHeight: 25,
    fontSize: 18,
  },
  scan: {
    backgroundColor: colors.yellow,
  },
});
