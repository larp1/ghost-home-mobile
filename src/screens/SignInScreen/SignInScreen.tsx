import { DefaultLayout } from 'layouts';
import { AppRoutes } from 'navigation';
import { SignInScreenProps } from 'navigation/MainStackNavigator';
import React, { FC, useState } from 'react';
import { ActivityIndicator, Alert, StyleSheet, View } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { actions, useAppDispatch } from 'store';
import { colors } from 'styles';
import { AppText } from 'ui/AppText/AppText';

export const SignInScreen: FC<SignInScreenProps> = ({ navigation }) => {
  const dispatch = useAppDispatch();
  const [isLoading, setIsLoading] = useState(false);

  const onScan = async (userId: string) => {
    try {
      setIsLoading(true);
      await dispatch(actions.viewer.login(Number(userId))).unwrap();
      navigation.reset({ routes: [{ name: AppRoutes.HomeScreen }] });
    } catch (err) {
      Alert.alert('', JSON.stringify(err));
      console.log('err', err);
      setIsLoading(false);
    }
  };

  return (
    <DefaultLayout withHorizontalPadding={false}>
      <AppText style={styles.head}>Дом с привидениями?</AppText>
      {isLoading ? (
        <View style={styles.flex}>
          <ActivityIndicator size="large" color={colors.text} />
        </View>
      ) : (
        <QRCodeScanner onRead={(e) => onScan(e.data)} />
      )}
    </DefaultLayout>
  );
};

const styles = StyleSheet.create({
  head: {
    fontSize: 30,
    fontWeight: '700',
    textAlign: 'center',
  },
  flex: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
